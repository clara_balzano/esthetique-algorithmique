var img;
var gui = new dat.GUI();
var params = {
    Nb_mainCircles: 100,
    random_Seed: 30,
    Opacity: 20,
};
gui.add(params, "random_Seed", 0, 100, 1);
gui.add(params, "Nb_mainCircles", 0, 200, 1);
gui.add(params, "Opacity", 0, 255, 1);
function draw() {
    background(0);
    randomSeed(params.random_Seed);
    noStroke();
    translate(width / 2, height / 2);
    var Nb_secondaryCircles;
    var tabMainCircleX = [];
    var tabMainCircleY = [];
    for (var i = 0; i < params.Nb_mainCircles; i++) {
        tabMainCircleX[i] = random(-width / 2 + 50, width / 2 - 180);
        tabMainCircleY[i] = random(-height / 2 + 50, height / 2 - 180);
        fill('white');
        ellipse(tabMainCircleX[i], tabMainCircleY[i], 25, 25);
        Nb_secondaryCircles = random(0, 100);
        var direction = [0, 0, 1, 1, 1, 1, 1, 1];
        var horizontal = random(direction);
        var offset = random(10, 50);
        var opacity = params.Opacity;
        for (var j = 0; j < Nb_secondaryCircles; j++) {
            fill(255, 255, 255, opacity);
            if (horizontal == 1) {
                ellipse(tabMainCircleX[i] + offset, tabMainCircleY[i], 40, 20);
            }
            else {
                ellipse(tabMainCircleX[i], tabMainCircleY[i] + offset, 20, 40);
            }
            offset += 2;
            opacity -= 0.7;
        }
    }
    blendMode(MULTIPLY);
    tint(255, 70);
    image(img, -width / 2, -height / 2, width, height);
    blendMode(BLEND);
}
function preload() {
    img = loadImage("../img/noise2.png");
}
function setup() {
    p6_CreateCanvas();
}
function windowResized() {
    p6_ResizeCanvas();
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map