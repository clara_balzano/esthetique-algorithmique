var img;
var mainCircles = [];
var velocity = [-2, -1, -0.5, 0.5, 1, 2];
function preload() {
}
function setup() {
    p6_CreateCanvas();
    for (var p = 1; p < 150; p++) {
        var c = new MainCircle();
        mainCircles.push(c);
    }
}
function windowResized() {
    p6_ResizeCanvas();
}
function draw() {
    background(0);
    randomSeed(1);
    translate(-100, -100);
    for (var i = 0; i < mainCircles.length; i++) {
        var direction = [0, 1];
        var horizontal = random(direction);
        mainCircles[i].move(horizontal);
        mainCircles[i].show(0, 0, 0);
        var nbSecondaryX = 10 * exp(abs(mainCircles[i].vx));
        var nbSecondaryY = 10 * exp(abs(mainCircles[i].vy));
        if (horizontal == 1) {
            if (mainCircles[i].vx < 0) {
                for (var j = nbSecondaryX; j >= 0; j--) {
                    mainCircles[i].show(2 * j, 0, 140 + 3 * j);
                }
            }
            else {
                for (var j = nbSecondaryX; j >= 0; j--) {
                    mainCircles[i].show(-2 * j, 0, 140 + 3 * j);
                }
            }
        }
        else {
            if (mainCircles[i].vy < 0) {
                for (var j = nbSecondaryY; j >= 0; j--) {
                    mainCircles[i].show(0, 2 * j, 140 + 3 * j);
                }
            }
            else {
                for (var j = nbSecondaryY; j >= 0; j--) {
                    mainCircles[i].show(0, -2 * j, 140 + 3 * j);
                }
            }
        }
    }
}
var MainCircle = (function () {
    function MainCircle() {
        this.x = random(width + 100);
        this.y = random(height + 100);
        this.vx = random(velocity);
        this.vy = random(velocity);
    }
    MainCircle.prototype.move = function (xmove) {
        if (xmove == 0) {
            this.y = (this.y + 3 * this.vy + height + 200) % (height + 200);
            this.vx = 0;
        }
        else {
            this.x = (this.x + 3 * this.vx + width + 200) % (width + 200);
            this.vy = 0;
        }
    };
    MainCircle.prototype.show = function (offsetx, offsety, offsetOpacity) {
        noStroke();
        fill(255, 253, 245, 255 - offsetOpacity);
        ellipse(this.x + offsetx, this.y + offsety, 26);
    };
    return MainCircle;
}());
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map