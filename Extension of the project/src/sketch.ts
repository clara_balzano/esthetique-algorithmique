// -------------------
//  Parameters and UI
// -------------------

var img;
let mainCircles = [];
let velocity = [-2, -1, -0.5, 0.5, 1, 2];



// -------------------
//    Initialization
// -------------------

function preload() {

}

function setup() {
    p6_CreateCanvas();

    // how can we make this an infinite loop ?
    for (let p = 1; p < 150; p++) {
        let c = new MainCircle();
        mainCircles.push(c);
    }

}


function windowResized() {
    p6_ResizeCanvas()
}


function draw() {
    background(0);
    randomSeed(1);
    translate(-100, -100);

    for (let i = 0; i < mainCircles.length; i++) {

        let direction = [0, 1];
        let horizontal = random(direction);


        //Main Circles
        mainCircles[i].move(horizontal);
        mainCircles[i].show(0, 0, 0);


        //Secondary Circles
        let nbSecondaryX = 10 * exp(abs(mainCircles[i].vx));
        let nbSecondaryY = 10 * exp(abs(mainCircles[i].vy));

        //Circles that move towards the X axis
        if (horizontal == 1) {

            //From right to left
            if (mainCircles[i].vx < 0) {

                for (let j = nbSecondaryX; j >= 0; j--) {
                    mainCircles[i].show(2 * j, 0, 140 + 3 * j);
                }
            }
            //From left to right
            else {
                for (let j = nbSecondaryX; j >= 0; j--) {

                    mainCircles[i].show(-2 * j, 0, 140 + 3 * j);
                }
            }
        }

        //Circles that move towards the Y axis
        else {

            //From top to bottom
            if (mainCircles[i].vy < 0) {

                for (let j = nbSecondaryY; j >= 0; j--) {
                    mainCircles[i].show(0, 2 * j, 140 + 3 * j);
                }
            }
            //From bottom to top
            else {
                for (let j = nbSecondaryY; j >= 0; j--) {

                    mainCircles[i].show(0, -2 * j, 140 + 3 * j);
                }
            }

        }


    }


}

class MainCircle {
    constructor() {
        this.x = random(width + 100);
        this.y = random(height + 100);
        this.vx = random(velocity)
        this.vy = random(velocity);

    }

    move(xmove) {
        if (xmove == 0) {
            this.y = (this.y + 3 * this.vy + height + 200) % (height + 200);
            this.vx = 0;
        }
        else {
            this.x = (this.x + 3 * this.vx + width + 200) % (width + 200);
            this.vy = 0;
        }



    }

    show(offsetx, offsety, offsetOpacity) {

        noStroke();
        fill(255, 253, 245, 255 - offsetOpacity);
        ellipse(this.x + offsetx, this.y + offsety, 26);

    }

}

