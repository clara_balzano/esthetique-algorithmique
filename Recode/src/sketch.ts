// -------------------
//  Parameters and UI
// -------------------

var img;

const gui = new dat.GUI()
const params = {
    Nb_mainCircles: 100,
    random_Seed: 30,
    Opacity: 20,

}
gui.add(params, "random_Seed", 0, 100, 1)
gui.add(params, "Nb_mainCircles", 0, 200, 1)
gui.add(params, "Opacity", 0, 255, 1)



// -------------------
//       Drawing
// -------------------

function draw() {

    background(0)
    randomSeed(params.random_Seed)
    noStroke()
    translate(width / 2, height / 2)

    let Nb_secondaryCircles;
    let tabMainCircleX = [];
    let tabMainCircleY = [];

    for (let i = 0; i < params.Nb_mainCircles; i++) {

        tabMainCircleX[i] = random(-width / 2 +50, width / 2 - 180);
        tabMainCircleY[i] = random(-height / 2 + 50, height / 2 -180);

        fill('white')
        ellipse(tabMainCircleX[i], tabMainCircleY[i], 25, 25)



        Nb_secondaryCircles = random(0, 100);
        const direction = [0, 0, 1, 1, 1, 1, 1, 1];
        const horizontal = random(direction);
        let offset = random(10, 50);
        let opacity = params.Opacity;

        for (let j = 0; j < Nb_secondaryCircles; j++) {
            fill(255, 255, 255, opacity);

            if (horizontal == 1) {
                ellipse(tabMainCircleX[i] + offset, tabMainCircleY[i], 40, 20);
            }
            else {
                ellipse(tabMainCircleX[i], tabMainCircleY[i] + offset, 20, 40);
            }

            offset += 2;
            opacity -= 0.7;
        }


    }

    blendMode(MULTIPLY);
    tint(255, 70);
    image(img, -width / 2, -height / 2, width, height);
    blendMode(BLEND);


}

// -------------------
//    Initialization
// -------------------

function preload() {
    img = loadImage("../img/noise2.png")
}

function setup() {
    p6_CreateCanvas()
}

function windowResized() {
    p6_ResizeCanvas()
}